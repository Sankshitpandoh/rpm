#!/usr/bin/env node

let config = require('./config/config.json');
const shell = require('shelljs');
// const path = 'output';
const appConfig = require('./config/appConfig.json');
const { dir } = require('console');

function performBuilding() {
    for(let i = 2; i < process.argv.length; i++){
        targetValue =  process.argv[i];
        objectKeys.includes(targetValue) ?
        getRepo()
        :
        console.log(targetValue + " is not a key in config");
    };
};

function getRepo() {
    let gitUrl = config[targetValue].repo_url;
    let branch = config[targetValue].branch;
    repoFolderName = shell.exec(`basename ${gitUrl} .git`);
    shell.exec(`mkdir -p ${appConfig.clonePath}/rpmBuilder/${repoFolderName}`);
    shell.cd(`${appConfig.clonePath}/rpmBuilder/${repoFolderName}`);
    shell.exec(`[ -d ${config[targetValue].path_to_clone} ] &&  cd ${config[targetValue].path_to_clone} || mkdir ${config[targetValue].path_to_clone}`);
    shell.cd(`${config[targetValue].path_to_clone}`)
    shell.exec(`git clone --single-branch --branch ${branch} ${gitUrl}`);
    shell.cd(`${repoFolderName}`);
    shell.exec('pwd');
    shell.exec(`${config[targetValue].post_clone_sh_file} ${repoFolderName}`);
    getTemplates();
}

function getTemplates() {
    if(config[targetValue].preinstall_required) {
        shell.exec(`cd ${__dirname}/preInstallTemplates && touch temp.sh && cat ${targetValue}_config.sh >> temp.sh && cat ${config[targetValue].preinstall_template} >> temp.sh`);
        shell.exec(`cd ../../ && mkdir -p preinstall`);
        shell.exec(`mv ${__dirname}/preInstallTemplates/temp.sh ../../preinstall/preinstall.sh`);
        console.log(__dirname)
        // shell.cd(`${appConfig.clonePath}/rpmBuilder/${repoFolderName}`);
        // shell.exec(`touch temp.sh && cat ${targetValue}_config.sh >> temp.sh && cat ${config[targetValue].preinstall_template} >> temp.sh`);
        // shell.exec(`cd ../output && mkdir preInstall`);
        // shell.exec(`mv temp.sh ../output/preInstall `);
        // shell.cd(`../output/${config[targetValue].path_to_clone}/${repoFolderName}`);
    };

    if(config[targetValue].postinstall_required) {
        shell.exec(`cd ${__dirname}/postInstallTemplates && touch temp.sh && cat ${targetValue}_config.sh >> temp.sh && cat ${config[targetValue].postinstall_template} >> temp.sh`);
        shell.exec(`cd ../../ && mkdir -p postinstall`);
        shell.exec(`mv ${__dirname}/postInstallTemplates/temp.sh ../../postinstall/postinstall.sh`);
        console.log(__dirname)
    };

    performZipping();
};

function performZipping() {
    let folderName = repoFolderName.trim();
    shell.exec('pwd');
    shell.cd('../../');
    shell.exec(`[ -f ${folderName}.zip ] && mkdir -p ${appConfig.dumpDirectory}/${folderName} && mv ${folderName}.zip dump/${folderName}-${+Date.now()}.zip`);
    shell.exec(`zip -r ${folderName}.zip *`);
    shell.exec(`mv ${folderName}.zip ${appConfig.outputPath}`)
    shell.exec(`rm -rf *`);
    getCheckFiles(folderName);
};

function getCheckFiles(folderName) {
    shell.exec('pwd');
    shell.cd(`${appConfig.outputPath}`)
    shell.exec(`[ -f ${folderName}.txt ] &&  rm -r ${folderName}.txt `);
    shell.exec(`echo "MD5CHECKSUM" >> ${folderName}.txt`);
    shell.exec(`md5sum ${folderName}.zip >> ${folderName}.txt`);
    shell.exec(`echo "SHA256CHECKSUM" >> ${folderName}.txt`);
    shell.exec(`touch ${folderName}.txt && sha256sum ${folderName}.zip >> ${folderName}.txt`);
}

let targetValue;
let objectKeys = Object.keys(config);
let repoFolderName;

process.argv.length > 2 ?
performBuilding()
:
console.log("no keys passed");

LOGBASEDIR="/logs"
SERVICENAME="microservices"
REPONAME="citibank-integration"
LDNAME="custom-integrations"
mkdir -p $LOGBASEDIR/$LDNAME
LOGDIR=$LOGBASEDIR/$LDNAME
LOGFILE=$LOGDIR/"preinstall-$(date '+%Y-%m-%d:%H-%M').log"

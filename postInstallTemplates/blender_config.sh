LOGBASEDIR="/logs"
SERVICENAME="microservices"
REPONAME="citibank-integration"
LDNAME="custom-integrations"
mkdir -p $LOGBASEDIR/$LDNAME
LOGDIR=$LOGBASEDIR/$LDNAME
OUTLOGFILE=$LOGDIR/"npm-out-$(date '+%Y-%m-%d:%H-%M').log"
ERRLOGFILE=$LOGDIR/"npm-err-$(date '+%Y-%m-%d:%H-%M').log"
ECHOLOG=$LOGDIR/"echo-out-$(date '+%Y-%m-%d:%H-%M').log"

